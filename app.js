import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import fs from 'fs';
import http from 'http';
import https from 'https';

import userRoutes from './routes/userRoutes';
import testRoutes from './routes/testRoutes';

// Set certificates for HTTPS support.
const privateKey  = fs.readFileSync('certificates/server.key', 'utf8');
const certificate = fs.readFileSync('certificates/server.crt', 'utf8');
const credentials = {key: privateKey, cert: certificate};

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.use('/user', userRoutes);
app.use('/test', testRoutes);

const httpsServer = https.createServer(credentials, app);
const httpServer = http.createServer(app);
httpsServer.listen(8443);
httpServer.listen(8080);