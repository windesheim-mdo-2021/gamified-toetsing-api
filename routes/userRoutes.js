import auth from "../controllers/authenticationController";
import express from 'express';

const router = express.Router();

router.post('/login', auth.login);
router.post('/register', auth.register);
router.post('/verify', auth.getUser);

router.use(auth.authenticate);

module.exports = router;
