import func from "../controllers/testController";
import casus from "../controllers/casusController";
import question from "../controllers/questionController";
import option from "../controllers/optionController";
import express from 'express';
import auth from "../controllers/authenticationController";
import answer from "../controllers/answerController";

const router = express.Router();

router.use(auth.authenticate);
router.post('/create', func.create);
router.post('/update', func.update);

router.post('/:id/casus/questions/:questionid/option/create', option.create);
router.post('/:id/casus/questions/:questionid/option/update/:optionId', option.update);
router.get('/:id/casus/questions/:questionid/option/get/:optionId', option.getOne);
router.get('/:id/casus/questions/:questionid/options/get', option.get);

router.post('/:id/casus/question/create', question.create);
router.get('/:id/casus/:casusid/questions/get/:questionid', question.getOne);
router.get('/:id/casus/:casusid/questions/get', question.get);
router.post('/:id/casus/questions/update/:questionid', question.update);

router.post('/:id/casus/create', casus.create);
router.post('/:id/casus/:casusId/update', casus.update);
router.get('/:id/casus/:casusId', casus.getOne);
router.get('/:id/casus', casus.get);

router.get('/:id/answer/check/:optionid', answer.check)
router.post('/:id/answer/submit', answer.submit)

router.get('/:id', func.getOne);
router.get('/', func.get);

module.exports = router;
