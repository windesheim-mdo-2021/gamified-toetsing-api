import { DataTypes } from 'sequelize';
import { Connection } from "./connection";

export let Option;

(async () => {
    const sequelize = await (new Connection()).getConnection();

    Option = sequelize.define("Option", {
        // Model attributes are defined here
        answer: {
            type: DataTypes.TEXT
        },
        score: {
            type: DataTypes.INTEGER
        },
        question_id: {
            type: DataTypes.INTEGER
        }
    });

    (async () => {
        await sequelize.sync();
    })();
})();




