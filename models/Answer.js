import { DataTypes } from 'sequelize';
import { Connection } from "./connection";

export let Answer;

(async () => {
    const sequelize = await (new Connection()).getConnection();

    Answer = sequelize.define("Answer", {
        // Model attributes are defined here
        question_id: {
            type: DataTypes.INTEGER
        },
        option_id: {
            type: DataTypes.INTEGER
        },
        user_id: {
            type: DataTypes.INTEGER
        },
        test_id: {
            type: DataTypes.INTEGER
        }
    });

    (async () => {
        await sequelize.sync();
    })();
})();




