import { DataTypes } from 'sequelize';
import { Connection } from "./connection";

export let Casus;

(async () => {
    const sequelize = await (new Connection()).getConnection();

    Casus = sequelize.define("Casus", {
        // Model attributes are defined here
        title: {
            type: DataTypes.TEXT
        },
        test_id: {
            type: DataTypes.INTEGER
        },
        description: {
            type: DataTypes.TEXT
        }
    });

    (async () => {
        await sequelize.sync();
    })();
})();




