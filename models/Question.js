import { DataTypes } from 'sequelize';
import { Connection } from "./connection";

export let Question;

(async () => {
    const sequelize = await (new Connection()).getConnection();

    Question = sequelize.define("Question", {
        // Model attributes are defined here
        title: {
            type: DataTypes.TEXT
        },
        description: {
            type: DataTypes.TEXT
        },
        type: {
            type: DataTypes.ENUM('multipleChoice', 'open'),
            default: 'multipleChoice'
        },
        casus_id: {
            type: DataTypes.INTEGER
        }
    });

    (async () => {
        await sequelize.sync();
    })();
})();




