import { DataTypes } from 'sequelize';
import { Connection } from "./connection";

export let User;

(async () => {
    const sequelize = await (new Connection()).getConnection();

    User = sequelize.define("User", {
        // Model attributes are defined here
        username: {
            type: DataTypes.STRING,
            allowNull: false
        },
        password: {
            type: DataTypes.STRING
        },
        role: {
            type: DataTypes.STRING
        }
    });

    (async () => {
        await sequelize.sync();
    })();
})();




