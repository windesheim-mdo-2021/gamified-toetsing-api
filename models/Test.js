import { DataTypes } from 'sequelize';
import { Connection } from "./connection";

export let Test;

(async () => {
    const sequelize = await (new Connection()).getConnection();

    Test = sequelize.define("Test", {
        // Model attributes are defined here
        time: {
            type: DataTypes.STRING,
            allowNull: false
        },
        instructions: {
            type: DataTypes.TEXT
        },
        user_id: {
            type: DataTypes.INTEGER
        }
    });

    (async () => {
        await sequelize.sync();
    })();
})();




