import Sequelize from "sequelize";

export class Connection {
    constructor() {
        this.sequelize = null;
    }

    getConnection = async () => {
        this.sequelize = new Sequelize(
            process.env.DATABASE_NAME,
            process.env.DATABASE_USER,
            process.env.DATABASE_PASSWORD,
            {
                host: process.env.DATABASE_HOST,
                dialect: process.env.DATABASE_DIALECT,
                logging: (process.env.DEBUG_SQL === 'true')
            }
        );

        try {
            await this.sequelize.authenticate();
            console.log('Connection has been established successfully.');

            return this.sequelize;
        } catch (error) {
            console.log('Unable to connect to the database:', error);
        }
    };
}