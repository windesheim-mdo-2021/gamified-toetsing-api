import {Casus} from "../models/Casus";
import {Test} from "../models/Test";

exports.create = async (req, res) => {
    let {title, description, testId} = req.body;

    const user = req.user;

    // Check if user is authorized to manage.
    const test = await Test.findOne({
        where: {id: testId, user_id: user.id}
    });

    if(test == null) {
        res.sendStatus(401);
    }

    const casus = new Casus();
    casus.title = title;
    casus.description = description;
    casus.test_id = testId;

    await casus.save();
    res.json(casus);
};

exports.get = async (req, res) => {
    let {id} = req.params;

    Casus.findAll({
        where: {test_id: id}
    }).then((t) => {
        if(t) {
            res.json(t);
            res.send();
        } else {
            res.sendStatus(404);
        }
    });
};

exports.getOne = async (req, res) => {
    let {casusId, id} = req.params;
    Casus.findOne({
        where: {id: casusId, test_id: id}
    }).then((t) => {
        if(t) {
            res.json(t);
            res.send();
        } else {
            res.sendStatus(404);
        }
    });
};

exports.update = async (req, res) => {
    let {casusId, testId, title, description} = req.body;

    const user = req.user;

    // Check if user is authorized to manage.
    const test = await Test.findOne({
        where: {id: testId, user_id: user.id}
    });

    if(test == null) {
        res.sendStatus(401);
    }

    Casus.findOne({
        where: {id: casusId}
    }).then(async (casus) => {
        if(casus) {
            casus.title = title;
            casus.description = description;
            await casus.save();

            res.json(casus);
            res.send();
        } else {
            res.sendStatus(404);
        }
    });
};