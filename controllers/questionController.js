import {Question} from "../models/Question";
import {Casus} from "../models/Casus";

exports.create = async (req, res) => {
    let {casusId, title, description = ""} = req.body;

    Casus.findOne({
        where: {id: casusId}
    }).then(async (t) => {
        if(t) {
            const question = new Question();
            question.title = title;
            question.description = description;
            question.casus_id = casusId;
            await question.save();

            res.json(question);
            res.send();
        } else {
            res.sendStatus(404);
        }
    });
};

exports.get = async (req, res) => {
    let {casusid} = req.params;

    Casus.findOne({
        where: {id: casusid}
    }).then(async (t) => {
        if(t) {
            const result = await Question.findAll({
                where: {casus_id: casusid}
            })

            res.json(result);
            res.send();
        } else {
            res.sendStatus(404);
        }
    });
};

exports.getOne = async (req, res) => {
    let {casusid, questionid} = req.params;

    Casus.findOne({
        where: {id: casusid}
    }).then(async (t) => {
        if(t) {
            const result = await Question.findOne({
                where: {id: questionid}
            })

            res.json(result);
            res.send();
        } else {
            res.sendStatus(404);
        }
    });
};

exports.update = async (req, res) => {
    let {questionId, title, description = ""} = req.body;

    Question.findOne({
        where: {id: questionId}
    }).then(async (q) => {
        if(q) {
            q.title = title;
            q.description = description;
            await q.save();

            res.json(q);
            res.send();
        } else {
            res.sendStatus(404);
        }
    });
};
