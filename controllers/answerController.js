import {Test} from "../models/Test";
import {Question} from "../models/Question";
import {Answer} from "../models/Answer";
import {Casus} from "../models/Casus";
import {Option} from "../models/Option";

exports.submit = async (req, res) => {
    let {testId, questionId, casusId, optionId} = req.body;
    const user = req.user;

    Test.findOne({
        where: {
            id: testId
        }
    }).then(async (t) => {
        Casus.findOne({
            where: {
                id: casusId,
                test_id: t.id
            }
        }).then(async (c) => {
            Question.findOne({
                where: {
                    id: questionId,
                    casus_id: c.id
                }
            }).then(async (q) => {
                Option.findOne({
                    where: {
                        id: optionId,
                        question_id: q.id
                    }
                }).then(async (o) => {
                    let answer = await Answer.findOne({
                        where: {
                            question_id: q.id,
                            test_id: t.id,
                            user_id: user.id,
                        }
                    });

                    if (answer) {
                        answer.option_id = o.id;
                    } else {
                        answer = await new Answer;
                        answer.option_id = o.id;
                        answer.test_id = t.id;
                        answer.user_id = user.id;
                        answer.question_id = q.id;
                    }
                    await answer.save();

                    res.json(answer);
                    res.send()
                })
            })
        })
    })
};

exports.check = async (req, res) => {
    let {optionid} = req.params;
    const user = req.user;

    const match = await Answer.findOne({
        where: {
            user_id: user.id,
            option_id: optionid
        }
    })
    if (match) {
        res.json(true);
    } else {
        res.json(false);
    }
    res.send();
}