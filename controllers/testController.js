import {Test} from "../models/Test";

exports.create = async (req, res) => {
    let {time, instructions} = req.body;

    const user = req.user;

    const test = new Test();
    test.time = time;
    test.instructions = instructions;
    test.user_id = user.id;
    await test.save();
    res.json(test);
};

exports.update = async (req, res) => {
    let {id, time, instructions} = req.body;

    Test.findOne({
        where: {id}
    }).then(async (t) => {
        if(t) {
            t.time = time;
            t.instructions = instructions;
            await t.save();

            res.json(t);
            res.send();
        } else {
            res.sendStatus(404);
        }
    });
};

exports.getOne = async (req, res) => {
    let {id} = req.params;

    Test.findOne({
        where: {id}
    }).then((t) => {
        if(t) {
            res.json(t);
            res.send();
        } else {
            res.sendStatus(404);
        }
    });
};

exports.get = async (req, res) => {
    Test.findAll({
        where: {'user_id': req.user.id}
    }).then((t) => {
        if(t) {
            res.json(t);
            res.send();
        } else {
            res.sendStatus(404);
        }
    });
};