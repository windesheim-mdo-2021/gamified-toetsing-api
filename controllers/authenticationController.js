import jwt from 'jsonwebtoken';
import {User} from '../models/User';

const accessTokenSecret = 'youraccesstokensecret';

const refreshTokenSecret = 'yourrefreshtokensecrethere';
let refreshTokens = [];

exports.login = async (req, res) => {
    const {username, password} = req.body;
    // const user = User.find(u => { return u.username === username && u.password === password });

    let user = await User.findAll({
        where: {
            username: username
        }
    });

    if(user.length === 1) {
        user = user[0];

        const bcrypt = require('bcrypt');
        bcrypt.compare(password, user.password, function(err, result) {
            if(result === true) {
                const accessToken = jwt.sign({username: user.username, role: user.role}, accessTokenSecret, { expiresIn: '1h' });
                const refreshToken = jwt.sign({username: user.username, role: user.role}, refreshTokenSecret);
                refreshTokens.push(refreshToken);

                res.json({
                    user: {id: user.id, username: user.username},
                    accessToken,
                    refreshToken
                })
            } else {
                res.send({"error": "Unknown user"});
            }
        });
    } else {
        res.send({"error": "Unknown user"});
    }
};

exports.register = async (req, res) => {
    let {username, password} = req.body;

    const bcrypt = require('bcrypt');
    const saltRounds = 10;

    await bcrypt.hash(password, saltRounds, function(err, hash) {
        const user = new User();
        user.username = username;
        user.password = hash;
        user.save();

        res.json("1");
    });

};

exports.getUser = async (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }

            User.findOne({
                where: {
                    username: user.username
                }
            }).then((u) => {
                res.json({
                    user: {id: u.id, username: u.username}
                });

                res.send();
            });

        });
    } else {
        res.sendStatus(401);
    }
};

exports.authenticate = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }

            User.findOne({
                where: {
                    username: user.username
                }
            }).then((u) => {
                req.user = u;
                next();
            });
        });
    } else {
        res.sendStatus(401);
    }
};

exports.refresh = (req, res) => {
    const {token} = req.body;

    if (!token) {
        return res.sendStatus(401);
    }

    if (!refreshTokens.includes(token)) {
        return res.sendStatus(403);
    }

    jwt.verify(token, refreshTokenSecret, (err, user) => {
        if (err) {
            return res.sendStatus(403);
        }

        const accessToken = jwt.sign({username: user.username, role: user.role}, accessTokenSecret, {expiresIn: '1h'});
        const refreshToken = jwt.sign({username: user.username, role: user.role}, refreshTokenSecret);
        refreshTokens.push(refreshToken);

        res.json({
            accessToken,
            refreshToken
        });
    });
};

exports.logout = (req, res) => {
    const { token } = req.body;
    refreshTokens = refreshTokens.filter(t => t !== token);

    res.send("Logout successful");
};