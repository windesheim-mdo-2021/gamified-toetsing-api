import {Question} from "../models/Question";
import {Option} from "../models/Option";

exports.create = async (req, res) => {
    console.log("JAJAJAJAJAJAJAJA", req.body);
    let {questionId, answer, score = 0} = req.body;

    Question.findOne({
        where: {id: questionId}
    }).then(async (q) => {
        if(q) {
            const option = new Option();
            option.answer = answer;
            option.score = score;
            option.question_id = q.id;

            await option.save();

            res.json(option);
            res.send();
        } else {
            res.sendStatus(404);
        }
    });
};

exports.get = async (req, res) => {
    let {questionid} = req.params;

    Question.findOne({
        where: {id: questionid}
    }).then(async (q) => {
        if(q) {
            const result = await Option.findAll({
                where: {question_id: q.id}
            })

            res.json(result);
            res.send();
        } else {
            res.sendStatus(404);
        }
    });
};

exports.getOne = async (req, res) => {
    let {questionid, optionId} = req.params;

    Question.findOne({
        where: {id: questionid}
    }).then(async (q) => {
        if(q) {
            const result = await Option.findOne({
                where: {id: optionId}
            })

            res.json(result);
            res.send();
        } else {
            res.sendStatus(404);
        }
    });
};

exports.update = async (req, res) => {
    let {optionId, answer, score = 0} = req.body;

    Option.findOne({
        where: {id: optionId}
    }).then(async (o) => {
        if(o) {
            o.answer = answer;
            o.score = score;
            await o.save();

            res.json(o);
            res.send();
        } else {
            res.sendStatus(404);
        }
    });
};
